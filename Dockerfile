# This Dockerfile creates an image with all the libraries used to compile 
# the projects of MATH0471

FROM ubuntu:24.04
# avoids interactive prompt for "tzdata"
ENV DEBIAN_FRONTEND=noninteractive

# ------------------------------------------------------------------------------
# libraries
# ------------------------------------------------------------------------------

RUN apt-get -y update && apt-get install -y --no-install-recommends \
    build-essential \
    cmake \
    mpi-default-dev \
    openmpi-bin \
    libeigen3-dev \
    wget \
    ca-certificates \
  && rm -rf /var/lib/apt/lists/*

# ------------------------------------------------------------------------------
# get recent Eigen https://eigen.tuxfamily.org/
# ------------------------------------------------------------------------------

# RUN cd /opt \
#   && VERSION=3.4.0 \
#   && wget -q https://gitlab.com/libeigen/eigen/-/archive/${VERSION}/eigen-${VERSION}.tar.gz \
#   && tar xf eigen-${VERSION}.tar.gz \
#   && rm eigen-${VERSION}.tar.gz \
#   && ln -s $PWD/eigen-${VERSION} $PWD/eigen

# ENV INCLUDE=/opt/eigen:$INCLUDE

# ------------------------------------------------------------------------------
# get recent gmsh https://gmsh.info/
#   3.0.6 requires libxcursor & libxinerama at runtime (gmsh exec)  
#   4.8.4 requires also libgl1-mesa-glx, libglu1-mesa, libxft2, libfontconfig1
#         for the link to gmsh-sdk
#   ubuntu 24.04: libgl1-mesa-glx does not exist anymore...
#
# => I check the dependencies of gmsh in ubuntu:
# > apt-cache show gmsh | grep Depends
# Depends: libc6 (>= 2.34), libgmsh4.12t64 (>= 4.12.1+ds1)
# > apt-cache show libgmsh4.12t64 | grep Depends
# ... look through the libs...
# ------------------------------------------------------------------------------

RUN VERSION=4.13.1 \
  && apt-get -y update && apt-get install -y --no-install-recommends \
    wget \
    libxcursor-dev \
    libxinerama-dev \
    libglu1-mesa libgl1 libglx-mesa0 \
    libxft2 libfontconfig1 \
  && cd /opt \
  && wget -q http://gmsh.info/bin/Linux/gmsh-${VERSION}-Linux64-sdk.tgz \
  && tar -xf gmsh-${VERSION}-Linux64-sdk.tgz \
  && rm gmsh-${VERSION}-Linux64-sdk.tgz \
  && ln -s $PWD/gmsh-${VERSION}-Linux64-sdk $PWD/gmsh \
  && rm -rf /var/lib/apt/lists/* \
  && echo "/opt/gmsh/lib" > /etc/ld.so.conf.d/gmsh.conf && \
  ldconfig

ENV INCLUDE=/opt/gmsh/include:$INCLUDE
ENV LIB=/opt/gmsh/lib:$LIB
ENV PATH=/opt/gmsh/bin:$PATH
ENV PYTHONPATH=/opt/gmsh/bin:$PYTHONPATH

# ------------------------------------------------------------------------------
# cleaning cache
# ------------------------------------------------------------------------------

RUN rm -rf /var/lib/apt/lists/*
